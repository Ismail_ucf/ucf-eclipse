#Difference in Minutes between Ticket Created and Ticket Closed For all Users
SELECT 
	U.FULL_NAME AS FullName,
    U.USER_NAME AS UserName,
    ROUND(timestampdiff(MINUTE,T.CREATED, T.TIME_CLOSED)) AS 'TimeDiffMinutes'    
FROM IT_TICKET_DASH.USERS U
	JOIN IT_TICKET_DASH.HD_TICKET T ON U.ID=T.OWNER_ID
    JOIN IT_TICKET_DASH.HD_STATUS S ON T.HD_STATUS_ID=S.ID
		WHERE T.HD_STATUS_ID in (12,20,28,36,44,52,68,76);
		
#Average time to close ticket for all Users in Minutes
SELECT avg(T1.TimeDiffMinutes) FROM(
	SELECT 
	U.FULL_NAME AS FullName,
	U.USER_NAME AS UserName,         
	ROUND(timestampdiff(Minute,T.CREATED, T.TIME_CLOSED)) AS 'TimeDiffMinutes'    
	FROM IT_TICKET_DASH.USERS U
	JOIN IT_TICKET_DASH.HD_TICKET T ON U.ID=T.OWNER_ID
	JOIN IT_TICKET_DASH.HD_STATUS S ON T.HD_STATUS_ID=S.ID
		WHERE T.HD_STATUS_ID in (12,20,28,36,44,52,68,76)         
	)T1;

#Average time to close ticket for ONE User in Minutes
SELECT avg(T1.TimeDiffMinutes) FROM(
	SELECT 
	U.FULL_NAME AS FullName,
	U.USER_NAME AS UserName,         
	ROUND(timestampdiff(Minute,T.CREATED, T.TIME_CLOSED)) AS 'TimeDiffMinutes'    
	FROM IT_TICKET_DASH.USERS U
	JOIN IT_TICKET_DASH.HD_TICKET T ON U.ID=T.OWNER_ID
	JOIN IT_TICKET_DASH.HD_STATUS S ON T.HD_STATUS_ID=S.ID
		WHERE T.HD_STATUS_ID in (12,20,28,36,44,52,68,76)    
		AND U.USER_NAME={USERNAME}    
		)T1;

#Shows in days if the Ticket was cloased by the due date
# negative value is closed early
# positive value is closed late
#This can be expanded
SELECT 
	T.ID,
	U.USER_NAME,     
    CASE T.TIME_CLOSED
		WHEN '0001-01-01 00:00:00' THEN null
        ELSE timestampdiff(day, T.DUE_DATE, T.TIME_CLOSED)
	END AS Due
FROM IT_TICKET_DASH.HD_TICKET T
    JOIN IT_TICKET_DASH.USERS U ON T.OWNER_ID=U.ID
    WHERE T.TIME_CLOSED<>'0001-01-01 00:00:00' AND T.DUE_DATE<>'0001-01-01 00:00:00';
    
 #Shows in days if the Ticket was cloased by the due date
# negative value is closed early
# positive value is closed late
#This can be expanded to get the hours and minutes
#Expanded needs refining
SELECT 
	CASE t1.daysDue 
    WHEN 0 THEN t1.hoursDue		
    ELSE t1.daysDue
    END as dueDayHour,
    CASE t1.hoursDue
    WHEN 0 THEN t1.minutesDue
    ELSE t1.hoursDue
    END as dueHourMinute
FROM(
	SELECT 
		T.ID,
		U.USER_NAME,     		
        timestampdiff(day, T.DUE_DATE, T.TIME_CLOSED) as daysDue,
        timestampdiff(hour, T.DUE_DATE, T.TIME_CLOSED) as hoursDue,
        timestampdiff(minute, T.DUE_DATE, T.TIME_CLOSED) as minutesDue
	FROM IT_TICKET_DASH.HD_TICKET T
		JOIN IT_TICKET_DASH.USERS U ON T.OWNER_ID=U.ID
		WHERE T.TIME_CLOSED<>'0001-01-01 00:00:00' AND T.DUE_DATE<>'0001-01-01 00:00:00') t1;
		
#this is what I am currently working on: having a little trouble with the sql. I would like to get an #accurate amount of time for when the ticket was completed before the due date or after
#I would like this to be displayed ready to go as either the
#number of days or number of hours, or number of minutes away from the due date.
#Having a problem with that at the moment
SELECT 
		T.ID,
		U.USER_NAME,     		
        timediff(T.DUE_DATE, T.TIME_CLOSED) as daysDue      
	FROM IT_TICKET_DASH.HD_TICKET T
		JOIN IT_TICKET_DASH.USERS U ON T.OWNER_ID=U.ID
		WHERE T.TIME_CLOSED<>'0001-01-01 00:00:00' AND T.DUE_DATE<>'0001-01-01 00:00:00';
        
SELECT 
	Date(t1.daysDue) as late,
    t1.daysDue
FROM(
	SELECT 
		T.ID,
		U.USER_NAME,     		
        SUBTIME(T.DUE_DATE, TIME(T.TIME_CLOSED)) as daysDue
	FROM IT_TICKET_DASH.HD_TICKET T
		JOIN IT_TICKET_DASH.USERS U ON T.OWNER_ID=U.ID
		WHERE T.TIME_CLOSED<>'0001-01-01 00:00:00' AND T.DUE_DATE<>'0001-01-01 00:00:00') t1;

#Category based uses ID, query that will take a value NAME and ID pair to return ALL tickets #for that combo
#Uses Name of category, gets ID and reads from ticket using that ID
SELECT * FROM HD_TICKET t 
	JOIN HD_CATEGORY c on t.HD_CATEGORY_ID=c.ID
	JOIN HD_QUEUE q on c.HD_QUEUE_ID=q.ID
		WHERE c.ID=12 AND q.ID=2;

#Category based ONLY. Takes the category ID to return all tickets for that category
SELECT * FROM HD_TICKET t
	JOIN HD_CATEGORY c on t.HD_CATEGORY_ID=c.ID
		WHERE c.ID=12;

#HD_QUEUE can be used as a "location" but more of a department that reported it
SELECT CUSTOM_FIELD_VALUE1,CONCAT('INT',Q.ID) as NUMBER_COL,TITLE,SUMMARY,APPROVE_STATE,CREATED,TIME_CLOSED FROM HD_TICKET t
	JOIN HD_QUEUE q ON t.HD_QUEUE_ID=q.ID
		WHERE t.CUSTOM_FIELD_VALUE1='Incident' \G

SELECT * FROM HD_TICKET t
	JOIN HD_QUEUE q ON t.HD_QUEUE_ID=q.ID
		WHERE q.ID=2 \G

#USERS based Query, gets all Tickets Submitted by the USERS ID
SELECT * FROM HD_TICKET t
	JOIN USERS u ON t.SUBMITTER_ID=u.ID
		WHERE u.ID=17;

#Get all tickets based on current status pair with the query below
SELECT * FROM HD_TICKET t
	WHERE t.HD_STATUS_ID=9;

#Paird with the query above this can be used to get the Name and State in a dropdown or #other selectable list and THEN the query above can use the ID to get the tickets based on #status
SELECT ID, NAME, STATE FROM HD_STATUS;

#Gets all of the tickets currently in default status for the specified location
SELECT * FROM HD_TICKET T
	JOIN HD_STATUS S ON S.ID=T.HD_STATUS_ID
		WHERE S.ID = (SELECT Q.DEFAULT_STATUS_ID FROM HD_QUEUE Q WHERE Q.ID=2);


#----------------------------Search bar queries-----------------------------------
#Gets all of the Tickets that resembles the selected string value
#This will pull all of the tickets if ANY field resembles or matchs the search string
# THIS is MAINLY FOR THE Search Bar
#We can also make the table selection selectable as a check box or something 
SELECT * FROM HD_TICKET T
	WHERE T.TITLE LIKE '%{SearchString}%'
		OR T.SUMMARY LIKE '%{SearchString}%'
        OR T.CUSTOM_FIELD_VALUE0 LIKE '%{SearchString}%'
        OR T.CUSTOM_FIELD_VALUE1 LIKE '%{SearchString}%'
        OR T.CUSTOM_FIELD_VALUE2 LIKE '%{SearchString}%'
        OR T.CUSTOM_FIELD_VALUE3 LIKE '%{SearchString}%'
        OR T.CUSTOM_FIELD_VALUE4 LIKE '%{SearchString}%'
        OR T.CUSTOM_FIELD_VALUE5 LIKE '%{SearchString}%'
        OR T.CUSTOM_FIELD_VALUE6 LIKE '%{SearchString}%'
        OR T.CUSTOM_FIELD_VALUE7 LIKE '%{SearchString}%'
        OR T.CUSTOM_FIELD_VALUE8 LIKE '%{SearchString}%'
        OR T.CUSTOM_FIELD_VALUE9 LIKE '%{SearchString}%'
        OR T.CUSTOM_FIELD_VALUE10 LIKE '%{SearchString}%'
        OR T.CUSTOM_FIELD_VALUE11 LIKE '%{SearchString}%'
        OR T.CUSTOM_FIELD_VALUE12 LIKE '%{SearchString}%'
        OR T.CUSTOM_FIELD_VALUE13 LIKE '%{SearchString}%'
        OR T.CUSTOM_FIELD_VALUE14 LIKE '%{SearchString}%'
        OR T.CUSTOM_FIELD_VALUE15 LIKE '%{SearchString}%'
        	ORDER BY T.TITLE;
  
SELECT * FROM HD_STATUS S
	WHERE S.NAME LIKE '%{SearchString}'
		OR S.STATE LIKE '%{SearchString}'
			ORDER BY S.NAME;


