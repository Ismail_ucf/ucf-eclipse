package com.ucf.it_dash.ITDashboardREST;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;



import javax.ws.rs.QueryParam;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;


/**
 * Root resource (exposed at "myresource" path)
 */
@Path("query")
public class MyResource {

    
	static final String DB_URL = "jdbc:mysql://localhost/IT_TICKET_DASH";
	static final String DB_USERNAME = "r00t";
	static final String DB_PASSWORD = "r00t";
	Connection connection = null;
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	
	
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getIt(@QueryParam("retval") String retval,
    		@QueryParam("table") String table,
    		@QueryParam("column") String column,
    		@QueryParam("condition") String condition ) throws SQLException, ClassNotFoundException {
    		
    		Class.forName(JDBC_DRIVER);
    		connection = DriverManager.getConnection(DB_URL,DB_USERNAME,DB_PASSWORD);
    		
    		String queryString = "SELECT $retVal FROM $tableName WHERE $columnName = ? ";
    		if(column.isEmpty() || column == null) {
    			queryString = "SELECT $retVal FROM $tableName";
    		}
    		
    		String query = queryString.replace("$retVal", retval);
    		query = query.replace("$tableName",table);
    		PreparedStatement preparedQuery = connection.prepareStatement( query );
    		if(!column.isEmpty()) {
    		query = query.replace("$columnName", column);
    		preparedQuery.setString(1,condition);
    		}
    		
    		
    		//preparedQuery.setString(2,column);
    		//preparedQuery.setString(3,condition);
    		
    		ResultSet rs = preparedQuery.executeQuery();
    		try {
				JSONObject jsonResultSet = getJsonFromRs(rs);
				//System.out.println(jsonResultSet);
				return jsonResultSet.toString();
				
			} catch (JSONException e) {
				System.out.println(e);
				// TODO Auto-generated catch block
				return "Failed";
			}
    		
    }
    
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/tickets")
    public String getTickets(@QueryParam("type") String type) throws SQLException {
    	
    	try {
			Class.forName(JDBC_DRIVER);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection(DB_URL,DB_USERNAME,DB_PASSWORD);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	String queryString = "";
	
	switch (QueryType.valueOf(type)) {
		case LIST_OPEN:
			queryString = "SELECT CUSTOM_FIELD_VALUE1,CONCAT('INT',Q.ID) AS NUMBER_COL,TITLE,SUMMARY,APPROVE_STATE,CREATED "
    			+ "FROM HD_TICKET t JOIN HD_QUEUE q ON t.HD_QUEUE_ID=q.ID "
    			+ "WHERE t.CUSTOM_FIELD_VALUE1='Incident' AND t.APPROVE_STATE='open'";
			break;
		
		case LIST_CLOSED:
			queryString = "SELECT CUSTOM_FIELD_VALUE1,CONCAT('INT',Q.ID) AS NUMBER_COL,TITLE,SUMMARY,APPROVE_STATE,CREATED,TIME_CLOSED "
	    			+ "FROM HD_TICKET t JOIN HD_QUEUE q ON t.HD_QUEUE_ID=q.ID "
	    			+ "WHERE t.CUSTOM_FIELD_VALUE1='Incident' AND t.APPROVE_STATE='closed'";
				break;
			
		case LIST_ALL:
			queryString = "SELECT CUSTOM_FIELD_VALUE1,CONCAT('INT',Q.ID) AS NUMBER_COL,TITLE,SUMMARY,APPROVE_STATE,CREATED,TIME_CLOSED "
	    			+ "FROM HD_TICKET t JOIN HD_QUEUE q ON t.HD_QUEUE_ID=q.ID "
	    			+ "WHERE t.CUSTOM_FIELD_VALUE1='Incident'";
			break;
		
		case CATEGORY_PIE:
			queryString = "SELECT NAME, COUNT(*) AS 'COUNT' FROM HD_TICKET t\n" + 
					"	JOIN HD_CATEGORY c on t.HD_CATEGORY_ID=c.ID\n" + 
					"		GROUP BY NAME HAVING COUNT(*) > 10";
			break;
		
		case CATEGORY_GAUGE:
			queryString = "SELECT COUNT(*) AS 'Open Records' FROM HD_TICKET WHERE APPROVE_STATE = 'open'";
			break;
			
		case CATEGORY_BAR_OPEN:
			queryString = "SELECT u.user_name as User_Name, count(*) as Count FROM HD_TICKET t left join users u on u.id = t.owner_id WHERE t.APPROVE_STATE = 'open' GROUP BY u.user_name";
			break;
		
		case CATEGORY_BAR_CLOSED:
			queryString = "SELECT u.user_name as User_Name, count(*) as Count FROM HD_TICKET t left join users u on u.id = t.owner_id WHERE t.APPROVE_STATE = 'closed' GROUP BY u.user_name";
			break;
			
		default:
			queryString = "";
			break;
	}

    	
    	

			PreparedStatement preparedQuery = connection.prepareStatement( queryString );
			ResultSet rs = preparedQuery.executeQuery();
			try {
				
				JSONObject jsonResultSet = getJsonFromRs(rs);				
				return jsonResultSet.toString();
				
			} catch (JSONException e) {
				System.out.println(e);
				// TODO Auto-generated catch block
				return "Failed";
			}
		
    }
    
    public JSONObject getJsonFromRs(ResultSet rs) throws SQLException, JSONException {
		
    	JSONArray json = new JSONArray();
    	ResultSetMetaData rsmd = rs.getMetaData();
    	while(rs.next()) {
   
    	  int numColumns = rsmd.getColumnCount();
    	  JSONObject obj = new JSONObject();
    	  for (int i=1; i<=numColumns; i++) {
    	    String column_name = rsmd.getColumnName(i); 
    	    obj.put(column_name, rs.getObject(column_name));
    	  }
    	 
    	  json.put(obj);
    	}
    	
    	 JSONObject objFinal = new JSONObject();
   	 objFinal.put("data",json);
    	
    	return objFinal;
    	
    	
    	
    }
}
